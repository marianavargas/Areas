import math
def area_esfera(Radio):
    """Instituto Tecnológico de Costa Rica 
Ingeniería en Computadores
Lenguaje: Python 3.6.4
Autor: Mariana Vargas Ramírez
Juan Pablo Alvarado Villalobos
Versión:1.0.0
Entradas:Radio
Salidas:Area esfera
"""
    from math import pi# importar
    Area=4*pi*Radio**2
    return Area
def area_piramide_cuadrada(Lado,Altura):
    """Instituto Tecnológico de Costa Rica 
Ingeniería en Computadores
Lenguaje: Python 3.6.4
Autor: Mariana Vargas Ramírez
Juan Pablo Alvarado Villalobos
Versión:1.0.0
Entradas: Lado y Altura
Salidas: Area piramide base cuadrada
"""
    A_base=Lado**2
    Apotema= math.sqrt(Lado**2+Altura**2) #Apotema
    A_lateral=((Lado*Apotema)/2)*4
    A_total=A_lateral+A_basal
    return A_total
    
